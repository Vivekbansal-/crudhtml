package nearpe.controller;

import nearpe.model.StudentDetails;
import nearpe.service.StudentDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;
@Controller
@RequestMapping("/form")
public class FormController {


    @Autowired
    StudentDetailsService studentDetailsService;



    @RequestMapping("/user")
    public String Form() {
        return "form";
    }

    @RequestMapping("/addData")
    public String addData(@RequestParam(required = false) String name,
                          @RequestParam(required = false) int age,@RequestParam(required = false) String city,
                          @RequestParam(required = false) String contact_number)
    {
        StudentDetails sd=new StudentDetails();
        sd.setAge(age);
        sd.setName(name);
        sd.setCity(city);
        sd.setContact_number(contact_number);
        StudentDetails b=this.studentDetailsService.adddata(sd);
        return "table";

    }
    @RequestMapping(value = "/tas", method = RequestMethod.GET)
    public ModelAndView taskList() {

        ModelAndView modelAndView=new ModelAndView();
        modelAndView.addObject("task",studentDetailsService.findAll());
        modelAndView.setViewName("getall");
        return modelAndView;
    }

    @PostMapping(value="/updateData")
    public String updateIt(@RequestParam(required = false) long id,@RequestParam(required = false) String name,
                           @RequestParam(required = false) int age,@RequestParam(required = false) String city,
                           @RequestParam(required = false) String contact_number) {
        StudentDetails details=new StudentDetails();
        if(id!=0)
            details.setId(id);
        if(name!=null)
            details.setName(name);
        if(age!=0)
            details.setAge(age);
        if(contact_number!=null)
        {
            details.setContact_number(contact_number);
        }
        if(city!=null)
            details.setCity(city);
        System.out.println("working");
        studentDetailsService.updateRecord(details);
        return "redirect:/form/tas";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable long id) {
        studentDetailsService.deleteById(id);
        return "redirect:/form/tas";
    }

    @RequestMapping(value="/formredirect/{id}")
    public ModelAndView redirect(@PathVariable long id)
    {
        ModelAndView modelAndView=new ModelAndView();

        modelAndView.addObject("val",id);
        modelAndView.setViewName("editform");
        return modelAndView;
    }

}

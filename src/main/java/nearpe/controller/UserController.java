package nearpe.controller;

import com.fasterxml.jackson.core.io.JsonEOFException;
import nearpe.exception.ResourceNotFoundException;
import nearpe.model.AreaInfo;
import nearpe.model.StudentDetails;
import nearpe.service.AjaxService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    AjaxService dataService;

    @GetMapping("/get")
    public String indexform()
    {
        return "index";
    }

    @RequestMapping(value = "/showData", method = RequestMethod.GET)
    public @ResponseBody List<StudentDetails> taskList() {
        List<StudentDetails> res=dataService.findAll();
        return res;

    }

    @PostMapping("/saveUser")
    public @ResponseBody String saveUser(@RequestParam(required = false) String name,
                   @RequestParam(required = false) Integer age, @RequestParam(required = false) String city,
                   @RequestParam(required = false) String contact_number) throws JSONException
    {
        System.out.print("controller..............");
        StudentDetails s= dataService.saveStudent(name,age,city,contact_number);

        JSONObject resObj = new JSONObject();
        resObj.put("id", s.getId());
        resObj.put("name",s.getName());
        resObj.put("age",s.getAge());
        resObj.put("city",s.getCity());
        resObj.put("contact_number", s.getContact_number());
        return resObj.toString();
    }

    @DeleteMapping("/delete/{id}")
    public @ResponseBody void deleteCompany(@PathVariable long id) {
        dataService.dele(id);
    }


    @GetMapping(value = "/fin/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StudentDetails> findById(@PathVariable long id) {
            StudentDetails sds = dataService.findByI(id);
            System.out.print("check");
            return ResponseEntity.ok(sds);  // return 200, with json body

    }

    @PutMapping("/update/{id}")
    public @ResponseBody String updateUser(@PathVariable("id") long id,
                                         @RequestParam(required = false) String name,
                                         @RequestParam(required = false) Integer age, @RequestParam(required = false) String city,
                                         @RequestParam(required = false) String contact_number) throws JSONException
    {
        System.out.print("inside update controller..............");
        StudentDetails sp= dataService.updateUser(id,name,age,city,contact_number);

        JSONObject resOb = new JSONObject();
        resOb.put("id", sp.getId());
        resOb.put("name",sp.getName());
        resOb.put("age",sp.getAge());
        resOb.put("city",sp.getCity());
        resOb.put("contact_number", sp.getContact_number());
        return resOb.toString();
    }


    @GetMapping("/gethome")
    public String gethome()
    {
        return "home";
    }




}

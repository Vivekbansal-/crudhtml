package nearpe.repository;


import nearpe.model.StudentDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface StudentDetailRepository extends JpaRepository<StudentDetails, Long> {
  /*  @Query(value = "select * from booking where actual_agent_id = :agent_id and "
            + "(new_status & 1) = 1 and commission > 0 ", nativeQuery = true)
    List<BookingEntity> getPendingCommission(@Param("agent_id") long agentId);*/
}

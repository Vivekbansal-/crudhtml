package nearpe.service;

import nearpe.exception.ResourceNotFoundException;
import nearpe.model.AreaInfo;
import nearpe.model.StudentDetails;
import nearpe.repository.StudentDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AjaxService {

    @Autowired
    StudentDetailRepository studentDetailsRepository;

    public StudentDetails saveStudent(String name, Integer age, String city, String contact_number) {
        StudentDetails s = new StudentDetails();
        s.setName(name);
        s.setCity(city);
        s.setAge(age);
        s.setContact_number(contact_number);
        studentDetailsRepository.save(s);
        return s;
    }

    public List<StudentDetails> findAll() {
        List<StudentDetails> bes = (List<StudentDetails>) studentDetailsRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        return bes;
    }

    public void dele(long id) {
        studentDetailsRepository.deleteById(id);
    }


    public StudentDetails findByI(Long id) {
        StudentDetails sdf = studentDetailsRepository.findById(id).orElse(null);
        return sdf;
    }

    public StudentDetails updateUser(long id, String name, Integer age, String city, String contact_number) {
        StudentDetails stu = findByI(id);
        stu.setName(name);
        stu.setAge(age);
        stu.setCity(city);
        stu.setContact_number(contact_number);
        StudentDetails j = studentDetailsRepository.save(stu);
        return j;
    }
}

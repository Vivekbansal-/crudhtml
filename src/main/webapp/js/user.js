function saveUsers() {
    console.log("post");
    var name = $('#name').val();
    var age = $('#age').val();
    var city = $('#city').val();
    var contact_number = $('#contact_number').val();
    var dynamicURL = "";
    var methodName = "";
    var id = $('#id').val();
    if (id) {
        dynamicURL = "/update/" + id;
        methodName = "PUT";
    } else {
        dynamicURL = "/saveUser";
        methodName = "POST";
    }
    $.ajax({
        type: methodName,
        url: dynamicURL,
        dataType: "json",
        data: {
            name: name,
            age: age,
            city: city,
            contact_number: contact_number
        },
        success: function (response) {
            console.log(response);
            var finalResponse = JSON.stringify(response);
            home();
            $("#responseDiv").html("<b>Your response is: " + finalResponse + "</b>");
        },
        error: function (response) {
            console.log(response);
        }

    });
}


function getAll() {

    console.log("hey");
    var html;
    $.ajax({
        type: "GET",
        url: "/showData",

        success: function (result) {
            html += "<div class='table-responsive'>";
            html += "<table  class='table table-bordered table-striped'>";
            html += "<tr>" +
                "<th>ID</th>" +
                "<th>NAME</th>" +
                "<th>AGE</th>" +
                "<th>CITY</th>" +
                "<th>CONTACT</th>" +

                "</tr>"

            for (var i = 0; i < result.length; i++) {
                var row = result[i];
                var id = row.id;
                var name = row.name;
                var age = row.age;
                var city = row.city;
                var contact_number = row.contact_number;


                html += "<tr>" +
                    "<td width='25%'>" + id + "</td>" +
                    "<td width='25%'>" + name + "</td>" +
                    "<td width='25%'>" + age + "</td>" +
                    "<td width='25%'>" + city + "</td>" +
                    "<td width='25%'>" + contact_number + "</td>" +
                    "<td width='25%'>" + "<button type='button'  onclick='update(" + id + ")'>EDIT</button>" + "</td>" +
                    "<td width='25%'>" + "<button type='button'  onclick='deleteUser(" + id + ")'>DELETE</button>" + "</td>" +

                    "</tr>";
            }
            html += "</table>";
            html += "</div>";
            $("#show").html(html);
        },
        error: function (response) {
            console.log(response);
        }

    });

}


function deleteUser(id) {
    $.ajax({
        type: "DELETE",
        url: "/delete/" + id,
        dataType: 'json',
        success: function (result) {
            console.log(result);
            alert('row deleted');
            console.log('row deleted');
            home();

        },
        error: function (result) {
            console.log(result);
            console.log('e');
            alert('row deleted');
            home();
        }
    });
}

function update(id) {
    console.log('found id');

    /*  document.getElementById("id").style.display='block';*/
    $.ajax({
        type: "GET",
        url: "/fin/" + id,
        dataType: 'json',
        success: function (data) {
            console.log('working');
            getADD()
            $('#id').val(data.id);
            $('#name').val(data.name);
            $('#age').val(data.age);
            $('#city').val(data.city);
            $('#contact_number').val(data.contact_number);
        },
        error: function (error) {
            console.log(error);
        }
    });
}


function home() {
    console.log("hey");
    var html;
    $.ajax({
        type: "GET",
        url: "/showData",

        success: function (result) {
            html = "<div  class='container mt-5'>";
            html += "<div class='row'>";

            for (var i = 0; i < result.length; i++) {
                var row = result[i];
                var id = row.id;
                var name = row.name;
                var age = row.age;
                var city = row.city;
                var contact_number = row.contact_number;


                html += "<div class='col-3 lg-3 mb-2 ml-3 mr-5'>";
                html += "<div class='card shadow p-3 mb-5 bg-white rounded bg-white' style='width: 18rem'>";
                html += "<img class='card-img-top' src='https://i.pinimg.com/736x/97/41/7b/97417b6aa73c900d76b63e4860dbd9ff.jpg' alt='Card image cap'>";
                html += "<div class='card-body'>";
                html += "<div>";
                html += "<div class='d-flex mb-3 align-items-center'>";
                html += "<h5 class='card-title p-2 mr-auto '>" + 'NAME: ' + name + "</h5>";
                html += "<a class='card-link' onclick='update(" + id + ")'><i class='fa fa-pencil-square-o fa-lg' aria-hidden='true'></i></a>";
                html += "<a class='card-link'  onclick='deleteUser(" + id + ")'><i class='fa fa-trash-o fa-lg' aria-hidden='true'></i></a>";
                html += "</div>";
                html += "<h6 class='card-text'>" + 'AGE: ' + age + "</h6>";
                html += "<h6 class='card-text'>" + 'CITY: ' + city + "</h6>";
                html += "<h6 class='card-text'>" + 'CONTACT: ' + contact_number + "</h6>";

                html += "</div>";
                html += "</div>";
                html += "</div>";
                html += "</div>";

            }
            html += "</div>";
            html += "</div>";
            $("#show").html(html);
        },
        error: function (response) {
            console.log(response);
        }

    });
}

function getADD() {
    var html;
    html = '<form>' +

        '<div class="form-group ">' +
        '<input type="number" class="form-control" name="id" id="id" hidden>' +
        '</div>' +
        '<div class="form-group">' +
        '<label>Name:</label>' +
        '<input type="text" class="form-control"  placeholder="Enter your name" name="name" id="name" required>' +
        '</div>' +
        '<div class="form-group">' +
        '<label>AGE:</label>' +
        '<input type="number" class="form-control" name="age" placeholder="Enter age" id="age" required>' +
        '</div>' +
        ' <div class="form-group">' +
        '<label>CITY</label>' +
        '<input type="text" class="form-control" name="city" id="city" placeholder="Enter city" required>' +
        ' </div>' +
        ' <div class="form-group">' +
        ' <label>Contact:</label>' +
        ' <input type="text" class="form-control" name="contact_number" id="contact_number" placeholder="Enter your contact number" required>' +
        '</div>' +
        '<button type="button" class="btn btn-primary b2" onClick="saveUsers()">Save  </button>' +

        '</form>';

    $("#show").html(html);

}


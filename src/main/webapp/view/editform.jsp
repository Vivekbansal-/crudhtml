<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
<form action="/form/updateData" method="POST">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/form/tas"><h3>GetData</h3> <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/form/user"><h3>Add</h3></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/form/tas"><h3>Update</h3></a>
                </li>

            </ul>
        </div>
    </nav>
    <div class="container">

        <table class="table">
            <tr>
                <td><label><b>Id</b></label></td>
                <td><input type="number" value=${val} name="id" ></td>
            </tr>
            <tr>
                <td><label><b>Name</b></label></td>
                <td><input type="text" placeholder="Enter your name" name="name" id="name" required></td>
            </tr>

            <tr>
                <td><label><b>Age</b></label></td>
                <td><input type="number" placeholder="Enter your age" name="age" id="age" required></td>

            </tr>

            <tr>
                <td><label><b>City</b></label></td>
                <td><input type="text" placeholder="Enter your city" name="city" id="city" required></td>
            </tr>

            <tr>
                <td><label><b>Contact No.</b></label></td>
                <td><input type="text" placeholder="Enter your contact number" name="contact_number" id="cn" required></td>
            </tr>
        </table>


    </div>
    <center><button type="submit" id="editbtn" class="btn btn-success">Edit</button></center>
</form>

</body>
</html>
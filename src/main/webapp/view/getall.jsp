<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <head>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </head>


    <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/form/tas"><h3>GetData</h3> <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/form/user"><h3>Add</h3></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/form/tas"><h3>Update</h3></a>
                </li>

            </ul>
        </div>
    </nav>
        <table class="table table-bordered table-striped" id="gettable">
                <tr>
                    <th>ID</th>
                    <th>NAME</th>
                    <th>AGE</th>
                    <th>CITY</th>
                    <th>CONTACT NO.</th>
                </tr>
                <c:forEach var="j" items="${task}">

                    <tr>
                        <td>${j.id}</td>
                        <td>${j.name}</td>
                        <td>${j.age}</td>
                        <td>${j.city}</td>
                        <td>${j.contact_number}</td>
                        <td><a href="/form/formredirect/${j.id}">edit</a></td>
                        <td><a href="/form/delete/${j.id}">delete</a></td>


                    </tr>
                </c:forEach>

            </table>
    </body>
</html>
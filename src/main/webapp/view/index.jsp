<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="height=device-height, width=device-width, initial-scale=1.0,
     minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, target-density ,dpi=device-dpi">



    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
            integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
            crossorigin="anonymous"
    />
</head>
<body>
<div class="row">
    <div class="col-lg-6">
        <div class="header">
            <h1 class="header-title" style="text-align: center;">ADD DETAILS</h1>
        </div>
        <div class="container">
            <form>

                 <div class="form-group">
                    <hr>
                    <!-- <label><b>Id</b></label>-->
                     <input type="number"  name="id" id="id" hidden ><hr>
                    <label><b>Name</b></label>
                    <input type="text" placeholder="Enter your name" name="name" id="name" required><hr>

                    <label><b>Age</b></label>
                    <input type="number" placeholder="Enter your age" name="age" id="age" required>
                    <hr>

                    <label><b>City</b></label>
                    <input type="text" placeholder="Enter your city" name="city" id="city" required>
                    <hr>

                    <label><b>Contact No.</b></label>
                    <input type="text" placeholder="Enter your contact number" name="contact_number" id="contact_number" required>
                    <hr>
                     <button type="button" class="btn btn-primary b2" onclick="saveUsers()" >Save</button>
                 </div>

            </form>
            <button type="button" class="btn btn-primary b2" onclick="getAll()">SHOW DETAILS</button>
            <hr>
            <div id="show" class="container"></div></form>
            <%--<input type="button" value="alert" onclick="abc()"/>--%>

        </div>
    </div>

</div>
<script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"
></script>
<script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"
></script>
<script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"
        integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf"
        crossorigin="anonymous"
></script>
<script src="../js/user.js"></script>
</body>
</html>